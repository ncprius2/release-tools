module Slack
  autoload :TagNotification, 'slack/tag_notification'
  autoload :UpstreamMergeNotification, 'slack/upstream_merge_notification'
  autoload :Webhook, 'slack/webhook'
end
