# Major release

This documentation has been moved to <https://gitlab.com/gitlab-org/release/docs/blob/master/general/major.md>.

---

[Return to Guides](../README.md#guides)
