# GitLab QA

This documentation has been moved to <https://gitlab.com/gitlab-org/release/docs/blob/master/general/qa-checklist.md>.

---

[Return to Guides](../README.md#guides)
